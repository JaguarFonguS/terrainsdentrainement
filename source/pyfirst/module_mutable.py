# Nous avons deux fonctions dans ce module
#
# author: Aurélien Schutz
# created : 15/07/2019


def modifier_non_mutable(a,b):
    print("Le syper type de la variable est ",type(a));
    a = 2 * a;
    b = 3 * b;
    print("Le type de la variable après modif est ",type(a));
    print("Resultat obtenu ",a,b);

def modifier_mutable(a,b):
    print("Le syper type de la variable est ",type(a));
    a.append(8);
    b[0] = 6;
    print("Le type de la variable après modif est ",type(a));
    print("Resultat obtenu ",a,b)