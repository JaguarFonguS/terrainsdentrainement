# NOus créeon une seconde classe
# Ma première classe avec de l'héritage
#
# author: Aurélien SCHUTZ <aurelien.schutz.03@gmail.com>
# date created: 16/07/2019

# Nous allons hériter d'une classe et nous allons charger Personne
import ModulePersonne

class Employe(ModulePersonne.Personne):
    '''Un employé est une personne comme une autre'''

    #Début du constructeur
    def __init__(self):
        '''Crée une instance de Employe'''
        # <nous appelons en premier le constructeur de classe mère
        ModulePersonne.Personne.__init__(self);
        # Nous initialisons la prime de l'Employé
        self.prime = 0.0;
    # Fin du constructeur

    # Début de la saisie
    def saisie(self):
        '''Je demande à l'utilisateur'''
        # J'utilise la saisie d'origine
        ModulePersonne.Personne.saisie(self);
        # Je demande à l'utilisateur de renseigner la Prime
        self.prime = float(input("Prime : "));
    # Fin de la saisie

    # Début de l'affichage
    def affichage(self):
        '''Affiche le contenue des champs'''
        # Utilise l'affichage de la mère
        ModulePersonne.Personne.affichage(self);
        # J'affiche la prime de l'employé
        print("Sa prime est : ",self.prime );
    # Fin de l'affichage

    # Début de la copie
    def __copy__(self):
        '''Duplique'''
        # Créée un nouveau Employe
        q = Employe();
        q.nom = self.nom;
        q.age = self.age;
        q.salaire = self.salaire;
        # # Demande à la mère de se copier
        # q = Employe(ModulePersonne.Personne.__copy__(self));
        # Duplique le champ prime
        q.prime = self.prime;
    # Fin de la copie

