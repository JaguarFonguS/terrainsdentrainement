# Module Notes
# Exercice pour manipuler des dictionnaires
#
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 16/07/2019

def notes():

    # nombre d'items
    n = int(input("Nombre d'items : "));

    # crée un dictionnaire vide
    dico = {};

    # Demande de saisir les paires clés valeur
    for i in range(0,n):
        cle = input("Clé : ");
        valeur = float(input("Valeur : "));
        dico[cle] = valeur;

    # affiche la liste des clés
    print("Liste de clés : ",dico.keys());

    # affiche la liste des valeurs
    print("Liste des valeurs : ",dico.values());

    # Affichage des paires
    print("Affichage des paires de valeurs.")
    for (k,v) in dico.items():
        print(k,v);

    # Calcule la somme des valeurs entrées
    s = 0;
    for v in dico.valeurs():
        s = s + v;
    print("La somme complete est : ");
