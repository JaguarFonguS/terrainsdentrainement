#!/usr/bin/python3

import mechanicalsoup
import sys
import datetime
import random
from datetime import date


#identite="M.;BLAY;Pierre-Luc;blay.pierreluc@gmail.com;ALTEN;photos/BLAY_Pierre-Luc.jpg;1"
#identite="Mme;BLUMENTRITT;Amiele;ablumentritt215@gmail.com;ALTEN;photos/BLUMENTRITT_Amiele.jpg;1"
#identite="M.;CAZALET;Guillaume;guillaume.cazalet@hotmail.fr;ALTEN;photos/CAZALET_Guillaume.jpg;1"
#identite="M.;CHATELAIN;Mathieu;contact@mathieuchatelain.com;ALTEN;photos/CHATELAIN_Mathieu.jpg;1"
#identite="M.;COLLIN;Charly;emploi@bobbyblues.com;ALTEN;photos/COLLIN_Charly.jpg;1"
#identite="Mme;GUEGUEN;Nolwenn;gueguenbouchynolwenn@wanadoo.fr;ALTEN;photos/GUEGUEN_Nolwenn.jpg;1"
#identite="M.;IZRAILOWICZ;Julien;julien.izrailowicz@gmail.com;ALTEN;photos/IZRAILOWICZ_Julien.jpg;1"
#identite="M.;KERVIL;Ronan;ronankervil@hotmail.com;ALTEN;photos/KERVIL_Ronan.jpg;1"
#identite="Mme;LE GUELVOUIT;Lena;lenalg@hotmail.fr;ALTEN;photos/LE_GUELVOUIT_Lena.jpg;1"
#identite="M.;LHERMEROUT;Gabriel;gabriel.lhermerout@outlook.com;ALTEN;photos/LHERMEROUT_Gabriel.jpg;1"
#identite="Mme;NICOLAS;Aurélie;aurelie.nicolas29@yahoo.fr;ALTEN;photos/NICOLAS_Aurelie.jpg;1"
#identite="Mme;NOWAK;Gaelle;gaellenowak@hotmail.fr;ALTEN;photos/NOWAK_Gaelle.jpg;1"
identite="M.;SCHUTZ;Aurélien;aurelien.schutz.03@gmail.com;ALTEN;photos/SCHUTZ_Aurelien.jpg;1"
#identite="Mme;SEVESTRE;Tifenn;sevestretifenn@gmail.com;ALTEN;photos/SEVESTRE_Tifenn.jpg;1"

codes = [(17, 6, 3686), (19, 6, 4118), (21, 6, 3354), (24, 6, 1848), (26, 6, 8244), (1, 7, 8455), (8, 7, 4423), (9, 7, 6317), (11, 7, 7573), (15, 7, 7871), (22, 7, 3269), (24, 7, 6482), (26, 7, 8729), (29, 7, 5438), (5, 8, 5017), (7, 8, 9434), (13, 8, 5106), (20, 8, 9854), (3, 9, 9164)]
# Rading input args
if len(sys.argv) < 3:
	print("Arguments incorrects.")
	print("Utilisation: python3 quest_aprem.py [1 matin, 2 aprem] [poste]")
	exit()

periode = int(sys.argv[1])
pc_no = sys.argv[2]
# Searching of the code
i = 0
month = date.today().month
day = date.today().day
while True:
	code = codes[i][2]
	i = i + 1
	if codes[i][1] > month or (codes[i][1] == month and codes[i][0] > day):
		break
code = str(code)

time = datetime.datetime.now()
# time += random.randint(0,200)
timestamp = time.strftime("[%Y-%m-%d - %Hh%M]")

# Creation of the browser
browser = mechanicalsoup.StatefulBrowser()
# Opening main page
url_base = "http://quest.ajc.razon.fr/"
url = "http://quest.ajc.razon.fr/2/index.php"
browser.open(url_base)
browser.open(url)
# Entering code
browser.select_form(nr=0)
browser["codeeval"] = code
browser.submit_selected()
# Selecting correct menu
browser.select_form(nr=1)
browser.submit_selected()
# Filling information and loging in
browser.select_form(nr=0)
browser["filiere_ii"]=identite
browser["periode"] = periode
browser["pc"]=pc_no
split_identite = identite.split(";")
browser["xcivilite"] = split_identite[0]
browser["civilite"] = split_identite[0]
browser["nom"] = split_identite[1]
browser["prenom"] = split_identite[2]
browser["email"] = split_identite[3]
browser["entreprise"] = split_identite[4]
browser["xnom"] = split_identite[1]
browser["xprenom"] = split_identite[2]
browser["xemail"] = split_identite[3]
browser["xentreprise"] = split_identite[4]


result = browser.submit_selected()
a = browser.get_current_page()
if str("Votre présence a bien été enregistrée.") in str(a):
	print(timestamp, "-", code, "-", "Votre présence a bien été enregistrée :)")
elif str("Vous avez déjà déclaré") in str(a):
	print(timestamp, "-", code, "-", "Vous avez déjà déclaré votre présence sur cette demi journée.")
else:
	print(timestamp, "-", code, "-", "Une erreur est survenue, enregistrez-vous manuellement :(")

print("")

