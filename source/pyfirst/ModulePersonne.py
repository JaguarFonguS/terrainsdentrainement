# Travail avec les classes
# Ma premier classe sous python
#
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# Date created: 16/07/2019

# Début de la définition
class Personne:
    """Modélise un humain d'après Sylvain"""

    # Début du constructeur
    def __init__(self):
        # Lister les champs
        self.nom = ""
        self.age = 0
        self.salaire = 0.0

    # Fin du constructeur

    # Début de la méthode de saisie des données
    def saisie(self):
        '''Permet de saisir les champs'''
        self.nom  = input("Nom : ");
        self.age = int(input("Age : "));
        self.salaire = float(input("Salaire : "));
    # Fin de la méthode de saisie des données

    # Début d'affichage des informations
    def affichage(self):
        '''Affiche le contenu de l'instance de l'objet'''
        print("Son nom est ", self.nom)
        print("Son âge est ", self.age)
        print("Son salaire est ", self.salaire)
    # Début d'affichage des informations

    # Début de la retraite
    def retraite(self, limite=62):
        reste = limite - self.age;
        if (reste < 0):
            print("Vous êtes retraité");
        else:
            print("Il vous reste %s années avant la retraite" % (reste) )
    # Fin de la retraite

    # Début de la duplication
    def __copy__(self):
        q = Personne();
        q.nom = self.nom;
        q.age = self.age;
        q.salaire = self.salaire;
        return q
    # Fin de la duplication

# Fin de la définition