# ## Tout premier code
# print("Hello world")
#
# ## L'initialisation et les boucles
# # Valeur limite
# n = int( input("n : "))
#
# #Inititialisation
# sommePaire = 0;
# sommeTotale = 0;
#
# #Effectuer la somme
# for i in range(1,n+1):
#     #somme si la valeur est paire
#     if (i % 2 == 0):
#         sommePaire = sommePaire + 1 ;
#     # on est plus dans le if ici, fin de bloc
#     # mais nous sommes toujours dans le bloc for
#     sommeTotale = sommeTotale + 1;
#
# #Nous sommes maintenant sortit du bloc for
# # Affichage sans transtypage
# print("Somme des valeurs paires :",sommePaire);
# #Affichage avec le transtypage explicite
# print("Somme des valeurs prises : " + str(sommeTotale));
#
# ## Bloc for contre les chaîne de caractères
# for i in "La séquence de texte complet":
#     #Comme demandé nous affichons
#     print(i)
#
# ## Premiers travaux sur l'import de fonctions externes
# import module_petit
#
# print(module_petit.petit(a=10,b=12))
#
# ## Les passage de fonctions
# import module_mutable
#
# # Appel de fonctions avec des valeurs par défaut
# x = 10;
# y = 15;
# module_mutable.modifier_non_mutable(a = x,b = y)
# print(x,y)
#
# # apple pour les listes
# lx = [10]
# ly = [15]
# module_mutable.modifier_mutable(a = lx,b = ly)
# print(lx,ly)
#
# ## Manipulations de listes
# L1 = [2,6,4];
# print(L1)
# print(L1.append([42,1]))
# print(L1.reverse())
#
# ## Manipulations avancées entre boucles et listes
# source = [1,5,8,12, 7]
# resultat = []
# for v in source:
#     resultat.append(v**2)
# print(resultat)
#
# resultat = []
# for v in source:
#     if (v % 2 == 0):
#         resultat.append(v**2)
# print(resultat)

## Somme de valeurs saisies par l'utilisateur
# demander le nombre d'obs
n = int( input("Nombre d'obs. = "))

#Créer une liste vide
lst = [];

# ajouter des valeurs
for i in range (0, n):
    lst.append(float(input("valeur = ")))

# effectuer la somme
somme = 0.0

for i in range(0,n):
    somme = somme + lst[i]

# affichage
print(somme)
