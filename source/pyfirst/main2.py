# Mardi 16 juillet 2019
# Test de commandes
#
# author: AUrélien Schutz <aurelien.schutz.03@gmail.com>
#
# ## Travail sur les dictionnaires
# d1 = {'Pierre':17,'Paul':15,'Jacques':16}
# print(d1)
#
# # accès par la valeur d'une clé
# print("La valeur associée à Paul est ",d1['Paul'])
#
# print(dir(d1))
#
# ## Test d'une focntions de prise de notes
# import module_notes
#
# module_notes.notes()

# ## Début du trav    il sur les classes
#
# # appel du module
# import ModulePersonne as MP
#
# # instanciation
# p = MP.Personne();
#
# # affiche tous les membres de p
# print(dir(p));
#
# # affectation aux champs
# p.nom = input("Nom : ");
# p.age = int(input("Age : "));
# p.salaire = float(input("Salaire : "));
#
# # affichage
# print(p.nom,", ",p.age,", ",p.salaire);
#
# # instanciation
# p2 = MP.Personne();
#
# # affiche tous les membres de p
# print(dir(p2));
#
# # affectation aux champs
# p2.nom = input("Nom : ");
# p2.age = int(input("Age : "));
# p2.salaire = float(input("Salaire : "));
#
# # affichage
# print(p2.nom,", ",p2.age,", ",p2.salaire);
#
# print(p,p2)
#
# ## Travaille les méthodes de la classe
#
# # Appel du module
# import ModulePersonne as MP
#
# # Instanciation
# p = MP.Personne();
# p0 = p;
# p2 = MP.Personne();
#
# # Saisie
# p.saisie();
# p2.saisie();
#
# # Méthode d'affichage
# p.affichage();
# p0.affichage();
# p2.affichage();
#
# print(p0,p,p2);
#
# ## Une méthode avec un argument en entrée
#
# # Appel du module
# import ModulePersonne as MP
#
# # Instanciation
# p = MP.Personne()
#
# # Saisie
# p.saisie();
#
# # Méthode affichage
# p.affichage();
#
# # Reste avant la retraite
# p.retraite();
#
# ## Accès indie et modification d'objets
#
# # nous allons faire une boucle de
# n = 3;
#
# # Appel du module
# import ModulePersonne as MP
#
# # Nous allons remplir gentiment
# liste = [];
# for i in range(0,3):
#     a = MP.Personne();
#     if i == 0:
#         a.nom = "Sylvain"
#         a.age = 39;
#         a.salaire = 3125.21;
#     elif i == 1:
#         a.nom = "Sébastien"
#         a.age = 3;
#         a.salaire = -123.40;
#     elif i == 2:
#         a.nom = "Aurélien"
#         a.age = 32;
#         a.salaire = 1054.50;
#
#     liste.append(a);
#
# i_entree = "0";
# while i_entree != "q":
#
#     # Saisir une entree
#     i_entree = input("N° ind. à traiter : ");
#
#     if (i_entree == "0") | (i_entree == "1") | (i_entree == "2") | (i_entree == "3") | (i_entree == "4") | (i_entree == "5") | (i_entree == "6") | (i_entree == "7") :
#         # L'entree est un numéro valide
#
#         # accès par numéro
#         numero = int(i_entree);
#
#         if (numero < len(liste)):
#             # le numéro est dans les index valables
#             b = liste[numero];
#
#             # Multiplie le salaire de la personne
#             b.salaire = b.salaire * 2;
#
#             # Affichage de nouveau
#             print("xxx début du seconde affichage")
#             for p in liste:
#                 print("------");
#                 p.affichage();
#         else:
#             print("Indice valable dans un autre contexte")
#     else:
#         # Npous n'avons pas un index mais surement un caractère queconque
#         print("    ")
#         print("Et tu dance dance")
#         print("    ")
#
# ## Une méthode qui est une fonction ( présence d'un return
#
# # Appel du module
# import ModulePersonne as MP
#
# # Instanciation
# p = MP.Personne();
#
# # Duplique notre objet
# a = p.__copy__();
#
# # Renseigne des informations
# a.nom = "Sébastien"
# a.age = 3;
# a.salaire = -123.40;
#
# # Affiche les onformations
# p.affichage();
# a.affichage();
#
## Travaillons sur l'héritage

# Appel du module
import ModulePersonne as MP
import ModuleEmploye as ME

# nous allons faire une boucle de
n = 4;

# Nous allons remplir gentiment
liste = [];
for i in range(0,n):
    if i == 0:
        a = MP.Personne();
        a.nom = "Sylvain"
        a.age = 39;
        a.salaire = 3125.21;
    elif i == 1:
        a = ME.Employe();
        a.nom = "Sébastien"
        a.age = 3;
        a.salaire = -123.40;
        a.prime = 250;
    elif i == 2:
        a = MP.Personne();
        a.nom = "Aurélien"
        a.age = 32;
        a.salaire = 1054.50;
    elif i == 3:
        a = ME.Employe();
        a.nom = "Gaëlle"
        a.age = 27;
        a.salaire = 2123.40;
        a.prime = 120;

    # Ajoute à la liste la personne
    liste.append(a);

# Affichage de la mort
for p in liste:
    print("--==--==--===---==-=-=-====-=-==-=");
    p.affichage();
print("--==--==--===---==-=-=-====-=-==-=");