# Troisième jour de cours python
# Le premier se nomme toto
#
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 17/07/2019
#
# ## Travailler le fichier texte 01
#
# # ouvrir le fichier en lecture
# f = open("ReineMarchesa","r");
#
# # Lecture
# s = f.read();
#
# # Affichage de ce qui est lu
# print("== Contenue de s ==")
# print(s)
# print("== Find de contenu ==")
#
# print(dir(s))
#
# # fermeture du fichier
# f.close();
#
# ## Travailler le fichier texte 02
#
# # ouvrir le fichier en lecture
# f = open("ReineMarchesa","r");
#
# # Lecture
# lst = f.readlines();
#
# # Affichage de ce qui est lu
# print("== Contenue de lst ==");
# print(lst[1]);
# print("== Find de contenu ==");
#
# # Informations sur lst
# print("Type de lst : ", type(lst));
# print("Longueur de lst : ", len(lst));
# print(dir(lst));
#
# # fermeture du fichier
# f.close();
#
# ## Travailler le fichier texte 03
#
# # ouvrir le fichier en lecture
# f = open("ReineMarchesa","r");
#
# while True:
#     # Nous lisons une ligne du fichier
#     s = f.readline();
#     if (s != ""):
#         # La ligne n'est pas vide
#         print(s);
#     else:
#         break;
#
# # fermeture du fichier
# f.close();
#
# ## Travailler le fichier texte 04
#
# # ouvrir le fichier en lecture
# f = open("ReineMarchesa","r");
#
# for s in f:
#     print(s,len(s));
#
# # fermeture du fichier
# f.close();
#
# ## ecrire le fichier texte 05
#
# # ouvrir le fichier en lecture
# f = open("moto.txt","w");
#
# # liste
# lst = ["honda\n","suzuki\n","benelli\n","yamaha\n","ducati"];
#
# # écriture de la liste
# f.writelines(lst);
#
# # fermeture
# f.close()
#
# ## ecrire le fichier texte 06
#
# # ouvrir le fichier en lecture
# f = open("moto.txt","a");
#
# # Ajoute un saut de ligne
# f.write(\n);
#
# # liste
# lst = ["honda\n","suzuki\n","benelli\n","yamaha\n","ducati"];
#
# # écriture de la liste
# f.writelines(lst);
#
# # fermeture
# f.close()
#
# for s in f:
#     print(s,len(s));
#
# # fermeture du fichier
# f.close();

## Enregistre au format json

# Importer les modules précédents
import ModulePersonne as MP
import json

# Saisie d'une personne
p = MP.Personne();
p.saisie();

# Sauvegarde
f = open("personne.json","w");

# Met sous forme d'un dictionnaire
d = {"Nom":p.nom,"Age":p.age,"Salaire":p.salaire}

# Sauver au format json
json.dump(d,f);

# Fermeture du fichier
f.close()