# Mon premier serveur web en utilisant python
#
# author:   Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 19/07/2019

# Importons un des modules instalés
from flask import Flask, render_template

# création d'un objet flask
app = Flask(__name__)

# Création d'un contrôleur
@app.route("/")

# Création d'une première page internet
def info():
    return render_template('deb.html', title="Virtus1er m'apprend le serveur web", my_string="Wheeee!")

# Test le programme avec l'option de déboguage
if __name__ == '__main__':
    app.run(debug=True)