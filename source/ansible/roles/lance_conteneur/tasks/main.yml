# Rôle ansible qui lance un conteneur en production
#
# author:       Aurélien J. Schutz <aurelien.schutz.03@gmail.com>
# date created: 22/08/2019

---

- name: Fermer les ports dans le pare-feu
  firewalld:
    port: "{{ ports[item] }}/tcp"
    permanent: no
    immediate: yes
    state: disabled
  loop: "{{ technologies  }}"

- name: install pip yum
  yum:
    name: python2-pip
  when: ansible_os_family == "RedHat"

- name: install pip apt
  apt:
    name: python-pip
  when: ansible_os_family == "Debian"

- name: Installe un module necessaire pour docker
  pip:
    name: docker

- name: Je m'assure que docker est lancé
  systemd:
    name: docker
    state: started

- name: Recupere les informations sur nos images disponibles
  docker_image_facts:
    name: "{{ depot.repertoire }}/{{ item }}"
  register: image_facts
  loop: "{{ liste_images }}"

- name: Stop un conteneur
  docker_container:
    name:  "{{ item['_ansible_item_label'] }}"
    state: stopped
    image: "{{ depot.repertoire }}/{{ item['_ansible_item_label'] }}"
  when: ( item['_ansible_item_label'] != "traefik" ) and ( 0 != ( item['images']|length ) )
  loop: "{{ image_facts['results'] }}"
  loop_control:
    label: "{{ item['_ansible_item_label'] }}"

- name: Détruit un conteneur
  docker_container:
    name:  "{{ item['_ansible_item_label'] }}"
    state: absent
    image:  "{{ depot.repertoire }}/{{ item['_ansible_item_label'] }}"
  when: ( item['_ansible_item_label'] != "traefik" ) and ( 0 != ( item['images']|length ) )
  loop: "{{ image_facts['results'] }}"
  loop_control:
    label: "{{ item['_ansible_item_label'] }}"

- name: Je demande d'enlever le tag latest des images si elles existent
  docker_image:
    state: absent
    name:  "{{ depot.repertoire }}/{{ item['_ansible_item_label'] }}"
    tag: latest
  when: ( item['_ansible_item_label'] != "traefik" ) and ( 0 != ( item['images']|length ) )
  loop: "{{ image_facts['results'] }}"
  loop_control:
    label: "{{ item['_ansible_item_label'] }}"

- name: Je m'assure que mes répertoires de travail existent
  file:
    path: "/home/ansible/{{ item }}"
    state: directory
    mode: 0755
    owner: ansible
    group: ansible
  loop: "{{ technologies  }}"

- name: Copie notre docker-compose.yml
  copy:
    src: "dc-{{ item }}.yml"
    dest: "/home/ansible/{{ item }}/docker-compose.yml"
    owner: ansible
    group: ansible
  loop: "{{ technologies  }}"

- name: Copie notre fichier de configuration traefik
  copy:
    src: "traefik.toml"
    dest: "/home/ansible/traefik/traefik.toml"
    owner: ansible
    group: ansible

- name: install docker-compose yum
  yum:
    name: docker-compose
  when: ansible_os_family == "RedHat"

- name: Lance notre commande docker compose
  shell: "docker-compose up -d --force-recreate"
  args:
    chdir: "/home/ansible/{{ item }}/"
  loop: "{{ technologies  }}"

- name: Associe notre conteneur traefik aux networks
  docker_network:
    name:  "{{ item }}_{{ conteneur[item] }}"
    connected:
      - traefik
    appends: yes
  loop: "{{ supernom_reseau }}"

- name: Ouvrir le port traefik dans le pare-feu
  firewalld:
    port: "{{ ports['traefik'] }}/tcp"
    permanent: no
    immediate: yes
    state: enabled

- name: Ajout des conteneurs dans le fichiers
  lineinfile:
    path: /etc/coredns/gaga.com.db
    regexp: "^{{ item }}.vps.gaga.com."
    line: "{{ item }}.vps.gaga.com. IN A {{ ansible_facts['default_ipv4']['address'] }}"
    state: present
  run_once: true
  loop: "{{ liste_images }}"
  delegate_to: "{{ groups['coredns'][0] }}"

- name: Redémarrer coredns
  delegate_to: "{{ groups['coredns'][0] }}"
  run_once: true
  systemd:
    name: coredns
    state: restarted

