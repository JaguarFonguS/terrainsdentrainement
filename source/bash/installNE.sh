#!/bin/bash
#
#
# author: Aurelien Schutz <aurelien.schutz.03@gmail.com>
# created date: 29/07/2019

export LANG=C
echo "*** begin installation job ***"

# ----------------------------------------------------------
# Initial Check
# ----------------------------------------------------------

CNT=0
# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
    ((CNT++))
    echo "ERROR${CNT}: This script must be run as root." 1>&2
fi
# exit if error
if [ $CNT -gt 0 ]; then
    echo "*** exit job ***" 1>&2
    exit 1
fi
echo "*** initial check OK ***"

# -----------------------------------------------------------
# Install of Node-Exporter
# -----------------------------------------------------------

# création d'un utilisateur supplémentaire
mkdir /opt/node-exporter
useradd -d /opt/node-exporter --shell /bin/false nodeexp

# création de quelques variables
BASE=https://github.com/prometheus/node_exporter/releases/download;
V=0.18.1

# Utilise curld pour récupérer le node exporter et l'extraire
curl -sL $BASE/v$V/node_exporter-${V}.linux-amd64.tar.gz|tar xvz -C /opt/node-exporter --strip-components=1

# Création d'un service node exporter
echo -e "[Unit]\nDescription=Node Exporter\nWants=network-online.target\nAfter=network-online.target\n">/usr/lib/systemd/system/node-exporter.service
echo -e "[Service]\nUser=nodeexp\nGroup=nodeexp\nType=simple\nExecStart=/opt/node-exporter/node_exporter\n">>/usr/lib/systemd/system/node-exporter.service
echo -e "[Install]\nWantedBy=multi-user.target">>/usr/lib/systemd/system/node-exporter.service

# charge notre nouveau service
systemctl daemon-reload
systemctl start node-exporter
systemctl enable node-exporter

