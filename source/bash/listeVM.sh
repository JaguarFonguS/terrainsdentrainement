#!/bin/bash
#
# Test sur les VM montées comprendre les ip
#
# author: AUrélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 	25/07/2019

# Liste les machines viruelles sur l'ordinateur
LISTEVM=$( virsh list --all | awk '{print $2}') ;

# Récupère l'adresse ip sur br0
IPMAITRE=$(nmcli connection show br0 | awk '$1=="ipv4.addresses:"{print $2}' | awk -F / '{print $1}')
#echo $IPMAITRE

# Essai de manipuler le fichier hosts
#echo $(cat /etc/hosts | awk -v "P=$IPMAITRE" '$1~/^P/{print $1}')
#echo $LISTEVM
#echo $(cat /etc/hosts | awk -v "P=$IPMAITRE" '$1~P{print $2}')

declare -a LISTEVM2;
declare -i I = 0;
for VM in $LISTEVM
do
	if [ "$VM" != "Name" ]
	then 
		LISTEVM2[$I]=$VM
		SHORT=$( echo $VM | sed 's/[0-9].*[0-9]$//' )
		# echo $SHORT
		LISTEVM2[$I]+="\t\t"$( cat /etc/hosts | awk -v "P=$SHORT" '$2~P{print $1}')"\n"
		# Augmente l'indice
		I+=1;
	fi
done

# echo -e $DISPLAYVM

select CHOIX in $DISPLAYVM2
do
	# suivant le choix, nous allons remplacer la VM
	echo -e $CHOIX
	break;
done
