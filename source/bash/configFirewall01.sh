#!/bin/bash
#
# author: Aurélien Schutz
# date created: 	30/07/2019
# some of the following commands may need you to be host

# installation de nslookup
yum install bind-utils

# un CentOS 7 I have located some log on authetication /var/log/secure
LIST=$(cat /var/log/secure | awk '$7=="authentication"{print $14}' | awk -F "=" '{print $2}' | sort -u );

#that will ask the firewall to add some ip sources to block zone. This zone bloc connection attempts 
# and ask the firewall to that that to the source. I do not know if that deny my access at this time
# somes attack is done with a dns name, that must be converted to an ip adress for firewalld computes it
LIST+=$( cat /var/log/secure | awk '$9=="root"{print $11}' | sort -u );
LIST+=$( cat /var/log/secure | awk '$9=="invalid"{print $13}' | sort -u );
for addr in $LIST;
do
	firewall-cmd --remove-source=$addr || firewall-cmd --remove-source=$(nslookup $addr | awk '$1=="Address:"{print $2}' | awk 'FNR == 2{print}' )
done

# # Saves the data stored
# firewall-cmd --zone=block --list-all >> /home/aurelien/Document/terrainsdentrainement/logs/attacks_$(date +%Y%m%d).log
