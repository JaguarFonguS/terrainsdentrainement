#!/bin/bash
#
# author: Aurélien Schutz
# date created: 	30/07/2019

## NOus allons ajouter plusieurs addresses bloquées
# some of the following commands may need you to be host

for addr in $(cat logs/attack-20190730.log | awk 'FNR == 5 {print}' | sed 's/source://' );
do
	firewall-cmd --zone=block --add-source=$addr
done

