#!/bin/bash
# Réaliser un script qui génère un chier index.html pour un répertoire
# donné pour les chiers jpg, png et html
# On pourra utiliser stat -c %y pour la date et du pour la taille
# 
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 04/07/2019

# donne une valeur par défaut avec le répertoire courant
r_initial=${1:-"$PWD"}

# afficher le titre
echo "Index de $r_initial"

# afficher la premiere ligne
echo "$r_initial"

# création d'un tableau
# feuille d'entête
echo -e "Type\tNom\t\tTaille\tDate de modification"

# je vais devoir faire une boucle sur les fichiers
for i_fichier in $r_initial/*
do
	#echo -e "\t$i_fichier\t$(du -h $i_fichier)\t$(stat -c %y $i_fichier)"
	echo -e "$(awk -F/ '{print $2}'<$i_fichier)"
done

