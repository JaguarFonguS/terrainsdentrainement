#!/bin/bash
# Sauvagarde ce que je fais dans la formation
#
# Aurélien Schutz <aurelien.schutz.03@gmail.com>
# created:	11/07/2019


cd /home/aurelien/Documents/
tar -czf TerrainsDentrainement.tar.gz TerrainsDentrainement/* TerrainsDentrainement/.git/*
cp /home/aurelien/Documents/TerrainsDentrainement.tar.gz /home/aurelien/Nextcloud/Projets/formationDevOps/TerrainsDentrainement.tar.gz

