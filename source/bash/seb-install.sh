#!/bin/bash

export LANG=C
echo "*** begin installation job ***"

# ----------------------------------------------------------
# Parameters
# ----------------------------------------------------------

#== Guest Name (Domain Name) ==
DOM=kube
#== Guest Image File ==
IMG=/mnt/vms/${DOM}.qcow2
#== Kickstart file ==
KSF=${DOM}-ks.cfg

#== num of vCPUs ==
VCPUS=1
#VCPUS=2
#== Guest Memory Size (MB) ==
RAM=4096
#RAM=2048
#== Guest Disk Size (GB) ==
SIZE=10.0
#SIZE=20.0
#== Virtual NETWORK ==
#VIRTUALNETWORK=bridge:br0
VIRTUALNETWORK=network:default

#== root password ==
PASSWORD=seb
#== Hostname ==
HOSTNAME=$DOM
#== IP Address ==
IP=192.168.1.90
#== Netmask ==
NETMASK=255.255.255.0
#== Gateway ==
GATEWAY=192.168.1.1
#== DNS server ==
#NAMESERVER=8.8.8.8,8.8.4.4
NAMESERVER=192.168.1.1
#== NTP server ==
NTPSERVERS=0.centos.pool.ntp.org,1.centos.pool.ntp.org,2.centos.pool.ntp.org,3.centos.pool.ntp.org
#NTPSERVERS=ntp1.jst.mfeed.ad.jp,ntp2.jst.mfeed.ad.jp,ntp3.jst.mfeed.ad.jp


#== Install Media (ISO) full path ==
DVD1=/home/seb/Téléchargements/CentOS-7-x86_64-Minimal-1810.iso
#DVD1=/var/lib/libvirt/images//CentOS-7-x86_64-Everything-1611.iso
#== dvd basename  ex. "CentOS-7-x86_64-DVD-1611.iso" ==
DVD1_ISO=$(basename ${DVD1})
#== dvd mount point name  ex. "CentOS-7-x86_64-DVD-1611" ==
DVD1_MNT=${DVD1_ISO%.*}

# ----------------------------------------------------------
# Initial Check
# ----------------------------------------------------------

CNT=0
# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
    ((CNT++))
    echo "ERROR${CNT}: This script must be run as root." 1>&2
fi
# Make sure dvd image file exists under /var/lib/libvirt/images dir
if [ ! -f ${DVD1} ]; then
    ((CNT++))
    echo "ERROR${CNT}: DVD install media [${DVD1_ISO}] must be copied under \"/var/lib/libvirt/images\" dir." 1>&2
fi
# exit if error
if [ $CNT -gt 0 ]; then
    echo "*** exit job ***" 1>&2
    exit 1
fi
echo "*** initial check OK ***"

# ----------------------------------------------------------
# erase previous vm & snapshot if existing
# ----------------------------------------------------------

echo "*** erase previous vm & snapshot ***"

# stop domain forcefully
virsh destroy ${DOM} >/dev/null 2>&1
# delete all snapshot of domain
virsh snapshot-list ${DOM} --name 2>/dev/null | xargs -I% sh -c "virsh snapshot-delete ${DOM} --snapshotname % >/dev/null 2>&1;"
# undefine domain
virsh undefine ${DOM} --remove-all-storage >/dev/null 2>&1
# remove domain image file
rm -f ${IMG} >/dev/null 2>&1

# ----------------------------------------------------------
# create kickstart file
# ----------------------------------------------------------

echo "*** kickstart file creating ***"

cat << _EOF_ > ${KSF}
#== Install OS instead of upgrade ==
install
#== Use CDROM installation media ==
cdrom
#== graphical or text install ==
text
#== shutdown after installation ==
shutdown
#poweroff
#halt
#reboot

#== System language ==
lang fr_FR.UTF-8
#lang ja_JP.UTF-8
#== Keyboard layouts ==
keyboard --vckeymap=fr-latin1 --xlayouts='fr_FR'
#keyboard --vckeymap=jp --xlayouts='jp'
#== System authorization information ==
auth --enableshadow --passalgo=sha512
#== Root password ==
rootpw --plaintext ${PASSWORD}
#== Run the Setup Agent on first boot ==
firstboot --disable
#== SELinux configuration ==
#-- selinux must be disabled before installation to avoid some error.
#selinux --enforcing
#selinux --permissive
selinux --disabled
#== Network information ==
network \
--device=eth0 \
--bootproto=static \
--ip=${IP} \
--netmask=${NETMASK} \
--gateway=${GATEWAY} \
--nameserver=${NAMESERVER} \
--noipv6 \
--activate \
--hostname=${HOSTNAME%%.*}
#== Firewall configuration ==
firewall --enabled --ssh
#firewall --enabled --ssh --http
#== System services
services --enabled="chronyd"
#== System timezone
timezone America/New_York --isUtc --ntpservers=${NTPSERVERS}
#timezone Asia/Tokyo --isUtc --ntpservers=${NTPSERVERS}

#== System bootloader configuration ==
bootloader --location=mbr --boot-drive=vda --append=" crashkernel=auto" 

#== use /dev/vda for install destination ==
ignoredisk --only-use=vda
#== Partition clearing information ==
clearpart --none --initlabel 
#== Disk partitioning information ==
part /boot --fstype="xfs" --ondisk=vda --size=500
part pv.2 --fstype="lvmpv" --ondisk=vda --size=8192 --grow
volgroup centos --pesize=4096 pv.2
logvol / --fstype="xfs" --grow --maxsize=51200 --size=1024 --name=root --vgname=centos
logvol swap --fstype="swap" --size=1024 --name=swap --vgname=centos

#== Packages Section ==
%packages
@^minimal
@core
chrony

#-- skip installing Adaptec SAS firmware
-aic94xx-firmware*
#-- skip installing firmware for wi-fi
-iwl*firmware
#-- skip installing firmware for WinTV Hauppauge PVR
-ivtv-firmware

%end

#== Addon Section ==
%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

#== Anaconda Section ==
%anaconda
pwpolicy root --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy luks --minlen=6 --minquality=50 --notstrict --nochanges --notempty
%end

#== Post Section ==
%post

%end

_EOF_

echo "*** kickstart file created ***"

# ----------------------------------------------------------
# guest install using virt-install command
# ----------------------------------------------------------

echo "*** virt-install starting ***"

virt-install \
--name="${DOM}" \
--ram=${RAM} \
--vcpus=${VCPUS} \
--cpu host \
--os-type=linux \
--os-variant=centos7.0 \
--file="${IMG}" \
--file-size=${SIZE} \
--location="${DVD1}" \
--network="${VIRTUALNETWORK}" \
--initrd-inject="${KSF}" \
--extra-args="ks=file:/${KSF} console=tty0 console=ttyS0 net.ifnames=0 biosdevname=0" \
--noautoconsole

if [ $? -ne 0 ]; then
  # something error happened before guest install
  exit 1
fi

# Display Console
virsh console ${DOM}

# wait until guest installation is completed
while true; do
  DOMSTATE=`virsh domstate ${DOM}`
  if [ "${DOMSTATE}" = "shut off" ]; then
    sleep 5
    break
  fi
  sleep 5
done

echo "*** virt-install finished ***"

# ----------------------------------------------------------
# begin customize guest
# ----------------------------------------------------------

echo "*** begin customize guest ***"

# --------------------------------------
# grub timeout settings
# --------------------------------------

echo "*** grub timeout settings ***"

F5R=/etc/default/grub
F5L=$(mktemp)
guestfish -d ${DOM} -i << _EOF_
  #== backup original
  cp-a ${F5R} ${F5R}-ORG
  #== copy from guest to local
  download ${F5R} ${F5L}
  #== edit on local
  ! sed -i -e 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/' ${F5L}
  ! sed -i -e '/GRUB_CMDLINE_LINUX=/s/ rhgb//g' ${F5L}
  ! sed -i -e '/GRUB_CMDLINE_LINUX=/s/ quiet//g' ${F5L}
  ! sed -i -e '/GRUB_CMDLINE_LINUX=/s/ console=[a-zA-Z0-9,]*//g' ${F5L}
  ! sed -i -e '/GRUB_CMDLINE_LINUX=/s/\"\$/ console=tty0 console=ttyS0\"/' ${F5L}
  ! echo ${F5L}
  ! cat ${F5L}
  #== copy from local to guest
  upload ${F5L} ${F5R}
  #== run grub2-mkconfig
  command "grub2-mkconfig -o /boot/grub2/grub.cfg"
_EOF_

# --------------------------------------
# swappiness settings
# --------------------------------------

echo "*** swappiness settings ***"

# swapiness settings file
F6R=/etc/sysctl.d/swappiness.conf

# suppress swappiness
guestfish -d ${DOM} -i << _EOF_
  # "tuned" must be disabled on centos7.2 for changing swappiness
  command "systemctl disable tuned.service"
  write ${F6R} "vm.swappiness = 0\n"
_EOF_

# --------------------------------------
# enable sshd, httpd services
# --------------------------------------

echo "*** enable sshd services ***"

guestfish -d ${DOM} -i << _EOF_
  command "systemctl enable sshd"
_EOF_


# ----------------------------
# /etc/hosts
# ----------------------------

echo "*** editing /etc/hosts ***"

F11R=/etc/hosts
guestfish -d ${DOM} -i << _EOF_
  #== backup original file
  cp-a ${F11R} ${F11R}-ORG
  #== update
  write-append ${F11R} "\n"
  write-append ${F11R} "${IP} ${HOSTNAME} ${HOSTNAME%%.*}\n"

_EOF_

# ----------------------------------------------------------
# end customize guest
# ----------------------------------------------------------

echo "*** finish all job ***"

# ----------------------------------------------------------
# EOJ
# ----------------------------------------------------------
