#!/bin/bash
# Un script qui demande Nom, Prénom, Sexe et âge et qi nous renvoie une synthese
#
# author : Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 03/07/2019

## section de debogguage
#PS4="--- PS4 est une console sony>"
#set -vx
## fin section de debogguage

PS3="Merci d'indiquer la réponse :"

#La première question porte sur le nom de la personne
v_nom=dupont;

echo "Pour le bien être de vos données personnelles, merci de m'indiquer votre nom"
while read line
do
	echo Je trouve votre nom très beau;
	echo Oui, en plus de stocker la donnée je me permet de juger;
	echo ;
	v_nom=$line;
	break;
done

#La deuxième question porte sur le prénom
v_prenom=henri

echo "Je vais vous demander votre prénom maintenant"
while read line
do
	echo Ben non, ce ne peux pas être $line votre prénom, c\'est pas compatible;
	echo ;
	v_prenom=$line;
	break;
done

#La troisième question porte sur le sexe
v_sexe=1;

echo "Continuons, merci de m'indiquer si vous êtes un :"
# commande qui semble afficher une liste d'arguments
select VAL in "homme" "femme"
do 
	case "$VAL" in
		homme)
			v_sexe=homme;
			break;;
		femme)
			v_sexe=femme;
			break;;
		*)
			echo "Les bonnes valeurs restent 1 ou 2" >&2
	esac
done

#La quatrième question porte sur l'âge de la personne
v_age=-1;

echo "Après, promis, je vous pose plus de question. Tu as quel âge ?"
while read line
do
	# inserer un test sur le fait que j'ai un entier
	if [ -z $line ]
	then
		# le line est vide
		echo "L'entrée est vide, merci de renseigner un âge"
	elif [ -e $line ]
	then
		# le fichier ou répertoire existe
		echo "Félicitation le je peux trouver l'objet !"
		echo "L'entrée est invalide" >&2
	elif [ $line -gt 0 ] && [ $line -lt 415 ]
	then
		# j'ai un entier compris strictement entre 0 et 415
		v_age=$line;
		break;
	else
		echo "Ceci n'est pas une entrée valide" >&2
	fi
done

echo ;
echo "Le grand frère, oui je suis mal traduit, c'est pareil."
if [ "$v_sexe" = "homme" ]
then
	echo "Le grand frère vient d'ajouter en base de donnée un $v_sexe de $v_age ans"
	echo "il se fait appeler $v_prenom $v_nom"
else
	echo "Le grand frère vient d'ajouter en base de donnée une $v_sexe"
	echo "il se fait appeler $v-prenom $v_nom"
echo "..."
echo "Non cela ne vas pas ensemble"
