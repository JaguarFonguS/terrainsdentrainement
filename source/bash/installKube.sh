#! /bin/bash
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
echo -e "[kubernetes]\nname=Kubernetes\nbaseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
	https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg">/etc/yum.repos.d/kubernetes.repo
yum install -y yum-utils device-mapper-persistent-data lvm2 yum-versionlock keepalived haproxy ipvsadm \
docker-ce docker-ce-cli containerd.io kubelet kubectl kubeadm
yum install -y https://packagecloud.io/linuxhq/hatop/packages/el/7/hatop-0.7.7-1.el7.centos.noarch.rpm/download.rpm
yum versionlock kubectl kubeadm kubelet
echo -e 'net.bridge.bridge-nf-call-iptables = 1\nvm.nr_hugepages = 0'>>/etc/sysctl.conf
sysctl --system
swapoff -a;sed -i '/swap/d' /etc/fstab
systemctl disable firewalld;systemctl stop firewalld
systemctl enable docker kubelet
systemctl start docker kubelet
kubeadm config images pull
kubeadm reset -f

