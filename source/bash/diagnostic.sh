#!/bin/bash
# Faire un script qui regarde tous les port ouverts de la machine et donne le type de service pour ce port.
#
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 03/07/2019

grandeListe() {
	for port in $(netstat -ant|awk '$6=="LISTEN"{print $4}'|sed 's/.*://')
	do 
		awk -v "U=$port/tcp" '$2~U{print $1}'</etc/services
	done
}

# grandeListe | sort -d | uniq -u
echo -e "Port\tService"
	for port in $(netstat -ant|awk '$6=="LISTEN"{print $4}'|sed 's/.*://'|sort -u)
	do 
		A=$(awk -v "U=$port/tcp" '$2~U{print $1}'</etc/services | sort -du)
		echo -en "$port\t"
		echo $A
	done
