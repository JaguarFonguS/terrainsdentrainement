#!/bin/bash
# Réaliser un script qui génère la table complète de multiplication de tous les chiffres de 1 à 10 
# en utilisant une boucle until et une boucle for.
# 
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 03/07/2019

if ! [ "$LANG" = "fr_FR.UTF-8" ]
then
	export LANG="fr_FR.UTF-8"
fi
entete() {
	local i
	local j
	ligne=\\u2554
	for ((j=0;j<5;j++))
	do 
		if [ $j -eq 4 ]
		then
			ligne=$ligne\\u2560
		elif ! [ $j -eq 0 ]
		then
			ligne=$ligne\\u2551

		fi;
		for ((i=0;i<11;i++))
		do
			if [ $j -eq 0 ] || [ $j -eq 4 ]
			then 
				ligne=$ligne\\u2550\\u2550\\u2550\\u2550\\u2550
			elif [ $j -eq 3 ]
			then
				case "$i" in
				0)
					ligne=$ligne\\u20\\u20\*\\u20\\u20;;
				1)
					ligne=$ligne\\u20\\u20\1\\u20\\u20;;
				2)
					ligne=$ligne\\u20\\u20\2\\u20\\u20;;
				3)
					ligne=$ligne\\u20\\u20\3\\u20\\u20;;
				4)
					ligne=$ligne\\u20\\u20\4\\u20\\u20;;
				5)
					ligne=$ligne\\u20\\u20\5\\u20\\u20;;
				6)
					ligne=$ligne\\u20\\u20\6\\u20\\u20;;
				7)
					ligne=$ligne\\u20\\u20\7\\u20\\u20;;
				8)
					ligne=$ligne\\u20\\u20\8\\u20\\u20;;
				9)
					ligne=$ligne\\u20\\u20\9\\u20\\u20;;
				10)
					ligne=$ligne\\u2010\\u20\\u20;;
				esac
			else
				ligne=$ligne\\u20\\u20\\u20\\u20\\u20
			fi;
			if [ $j -eq 0 ] && [ $i -eq 0 ] 
			then ligne=$ligne\\u2566
			elif [ $j -eq 0 ] && [ $i -eq 10 ]
			then
				ligne=$ligne\\u2557
			elif [ $j -eq 4 ] && [ $i -eq 0 ] 
			then ligne=$ligne\\u256C
			elif [ $j -eq 4 ] && [ $i -eq 10 ]
			then
				ligne=$ligne\\u2563
			elif [ $i -eq 0 ] || [ $i -eq 10 ]
			then
				ligne=$ligne\\u2551
			elif [ $j -eq 0 ]
			then
				ligne=$ligne\\u2564
			elif [ $j -eq 4 ]
			then
				ligne=$ligne\\u256A
			else
				ligne=$ligne\\u2502
			fi;
		done
		ligne=$ligne\\n
	done
}

clear

entete
echo -e $ligne
