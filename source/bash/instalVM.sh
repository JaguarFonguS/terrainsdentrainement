#!/usr/bin/env bash
#
# Le but de ce script installer plus plus automatiquement possible une machine virtuelle
#
# author: Aurélien Schutz <aurelien.schutz.03gmail.com>
# date created: 08/07/2019
# version: 0.1.1

clear

# Update kickstart file
#echo -en "Enter Hostname: "
#read HOST_NAME
#echo -en "Enter IP Address: "
#read IP_ADDRESS
## une fois utilisé, il n'est plus possible de corriger correctement le fichie kickstart
## sudo sed -i 's/gitlab03/'$HOST_NAME'/g' /srv/http/ks.cfg
## sudo sed -i 's/192.168.1.39/'$IP_ADDRESS'/g' /srv/http/ks.cfg
PATH_KS='/home/aurelien/Documents/TerrainsDentrainement/source/bash'
#cat "$PATH_KS/ks.cfg" | sed 's/gitlab03/'$HOST_NAME'/g' | sed 's/192.168.1.39/'$IP_ADRESS'/g' > "$PATH_KS/ks2.cfg"
 
## Pre-defined variables
# echo ""
MEM_SIZE=4096
VCPUS=1
OS_VARIANT="rhel7"
ISO_FILE='/home/aurelien/Téléchargements/CentOS-7-x86_64-Minimal-1810.iso'

# using HOST_NAME instead
#echo -en "Enter vm name: "
#read VM_NAME
#VM_NAME=$HOST_NAME
VM_NAME="toto"
OS_TYPE="linux"
echo -en "Enter virtual disk size : "
read DISK_SIZE
 
     # je voulais commenter la ligno pour le docker0 qui n'est pas configuré pour le moment
     #--network bridge=br0 --network bridge=docker0 \
     #--os-type=${OS_TYPE} \
     #--os-variant=${OS_VARIANT} \
     #--graphics=none \
     #--initrd-inject=${PATH_KS} \
     #-x "ks=file:/ks2.cfg" 
     #--console pty,target_type=serial \
     #-x 'console=ttyS0,115200n8 serial' \
sudo virt-install \
     --name ${VM_NAME} \
     --memory=${MEM_SIZE} \
     --vcpus=${VCPUS} \
     --location=${ISO_FILE} \
     --disk size=${DISK_SIZE}  \
     --network bridge=br0 \
     --graphics=none \
     --initrd-inject="${PATH_KS}/ks2.cfg" \
     -x 'console=ttyS0' \
     -x "ks=file:/ks2.cfg" 
