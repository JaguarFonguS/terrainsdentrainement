#!/bin/bash
# Copier le comportement de la fonction seq
#
# Synopsis
# seq [OPTION]... LAST
# seq [OPTION]... FIRST LAST
# seq [OPTION]... FIRST INCREMENT LAST
# Description
# 
# Print numbers from FIRST to LAST, in steps of INCREMENT.
# 
# -f, --format=FORMAT
#     use printf style floating-point FORMAT 
# -s, --separator=STRING
#    use STRING to separate numbers (default: \n) 
# -w, --equal-width
#    equalize width by padding with leading zeroes 
# --help
#    display this help and exit 
# --version
#     output version information and exit
# 
# If FIRST or INCREMENT is omitted, it defaults to 1. That is, an omitted INCREMENT defaults to 1 even when LAST is smaller than FIRST. FIRST, INCREMENT, and LAST are interpreted as floating point values. INCREMENT is usually positive if FIRST is smaller than LAST, and INCREMENT is usually negative if FIRST is greater than LAST. FORMAT must be suitable for printing one argument of type 'double'; it defaults to %.PRECf if FIRST, INCREMENT, and LAST are all fixed point decimal numbers with maximum precision PREC, and to %g otherwise. 
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 05/07/2019

if (($# == 0))
then
	echo "mySeq: opérande manquant"
	echo "Saisissez ♲"mySeq --help" pour plus d'informations"
fi

for arg in $*;
do
	case "$arg" in
	--help)
		echo "Utilisation : mySeq [OPTION]... DERNIER"
                echo "         ou : mySeq [OPTION]... PREMIER DERNIER"
                echo "         ou : mySeq [OPTION]... PREMIER PAS DERNIER"
                echo "Afficher les nombres de PREMIER jusqu'à DERNIER, avec ce PAS."
                echo ""
                echo "Les arguments obligatoires pour les options longues le sont aussi pour les"
                echo "options courtes."
                echo "  -f, --format=FORMAT      utiliser FORMAT comme style printf à"
                echo "                             virgule flottante"
                echo "  -s, --separator=CHAÎNE   utiliser CHAÎNE pour séparer les nombres"
                echo "                             (\n par défaut)"
                echo "  -w, --equal-width        équilibrer la largeur en remplissant l'en-tête par"
                echo "                             des zéros"
                echo "      --help     afficher l'aide et quitter"
                echo "      --version  afficher des informations de version et quitter"
                echo ""
                echo "Si PREMIER ou PAS sont omis, la valeur 1 est utilisée par défaut. La"
                echo "valeur PAS par défaut est 1 même lorsque DERNIER est plus petit que PREMIER."
                echo "La suite de nombres se termine quand la somme du nombre actuel et de PAS"
                echo "deviendrait plus grande que DERNIER."
                echo "PREMIER, PAS et DERNIER sont interprétés en notation à virgule flottante."
                echo "PAS est habituellement positif si PREMIER est plus petit que DERNIER et"
                echo "PAS est habituellement négatif si PREMIER est plus grand que DERNIER."
                echo "PAS ne peut pas être 0. Ni PREMIER ni PAS ni DERNIER ne peuvent être NaN"
                echo "(pas un nombre)."
                echo "FORMAT doit permettre d'afficher un argument de type « double » ;"
                echo "par défaut à %.PRECf si PREMIER, PAS et DERNIER sont tous décimaux en"
                echo "notation fixe avec une précision maximale PREC, et à %g sinon."
                echo ""
                echo "Aide en ligne de GNU coreutils : <https://www.gnu.org/software/coreutils/>"
                echo "Signalez les problèmes de traduction de « seq » à : <traduc@traduc.org>"
                echo "Documentation complète à : <https://www.gnu.org/software/coreutils/seq>"
                echo "ou disponible localement via: info '(coreutils) seq invocation'"
                echo "done";;
	--version)
                echo "mySeq 0.30"
                echo "Copyright © 2018 Free Software Foundation, Inc."
                echo "License GPLv3+ : GNU GPL version 3 ou ultérieure <https://www.gnu.org/licenses/gpl.fr.html>"
                echo "Ceci est un logiciel libre. Vous êtes libre de le modifier et de le redistribuer."
                echo "Ce logiciel n'est accompagné d'ABSOLUMENT AUCUNE GARANTIE, dans les limites"
                echo "permises par la loi."
                echo "Écrit par Aurélien Schutz.";;
	-f)
		echo "Travail d'écriture en cours" >&2;;
	-s)
		echo "Travail d'écriture en cours" >&2;;
	-w)
		echo "Travail d'écriture en cours" >&2;;
	*)
		echo "Travail d'écriture en cours";;
esac

