#!/bin/bash
# Faire un script qui affiche ceci (l'équivalent de la fonction id)
#
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 04/07/2019

% la variable va prendre en entrée soit $1 soit le contenu de $USER par défaut
n_utilisateur=${1:-$USER}

clear
echo -e "Utilisateur:\t$n_utilisateur"

# je commence par demander si l'utilisateur existe sur la machine 
grep -qi $n_utilisateur </etc/passwd || echo "L'utilisateur $n_utilisateur n'existe pas" >&2

echo -ne "Identifiant:\t"
# l'utilisateur existe, je veux récupérer une information dans le dossier /etc/passwd
awk -F: -v U=$n_utilisateur '$1==U{print $3}' </etc/passwd

echo -ne "Groupe:\t\t"
#l'identifiant du groupe principal, recuperons le
i_group=$( awk -F: -v U=$n_utilisateur '$1==U{print $4}' </etc/passwd )
awk -F: -v U=$i_group '$3==U{print $1}' </etc/group

echo -ne "Groupes:\t\t"
#récuperer chaque groupe pour l'utilisateur
awk -F: -v U=$n_utilisateur '$4==/U/{print $1}' </etc/group
