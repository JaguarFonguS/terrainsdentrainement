#! /bin/bash
#
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created:		07/08/2019

# Ajouter des adresse ip c'est pour doner des accès et c'est l'inverse que je veux
# Aussi nous allons tout bloquer c'est la zone block
cp /usr/lib/firewalld/zones/block.xml /usr/lib/firewalld/zones/aurelienblock.xml
firewall-cmd --reload
# J'ouvre le port pour l'application nodeJS
firewall-cmd --add-port=3000/tcp --zone=aurelienblock
# J'ouvre le port pour afficher Jenkins
firewall-cmd --add-port=8080/tcp --zone=aurelienblock
# Je veux pouvoir me connecter en ssh
firewall-cmd --add-service=ssh --zone=aurelienblock
# Lance le blocage
firewall-cmd --set-default-zone=aurelienblock
