#!/bin/bash
#
# author: Aurélien Schutz <aurelien.schutz.03@gmail.com>
# date created: 04/07/2019

# Recupere sur http://sdz.tdct.org/sdz/ersonnalisez-votre-shell-en-couleur.html
# Remplacé par http://bashrcgenerator.com/
#PS1='\
#\[\033[00m\][\
#\[\033[31m\]\u\
#\[\033[00m\]@\
#\[\033[35m\]\h\
#\[\033[00m\]:\
#\[\033[322m\]\w\
#\[\033[00m\]]\
#\[\033[00m\]▶\
# '
# export PS1="\[\033[38;5;22m\]\[\033[48;5;11m\]\H\[$(tput sgr0)\]"
sep=""
export PS1="\[\033[38;5;11m\]\[\033[48;5;22m\]\u@\h\[$(tput sgr0)\]\[\033[38;5;22m\]\[\033[48;5;11m\]$sep \W\[$(tput sgr0)\]\[\033[38;5;11m\]$sep\[$(tput sgr0)\]"

alias ls='ls --color=auto'
